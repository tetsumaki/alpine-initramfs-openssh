#!/bin/sh

set -- $(cat /proc/cmdline)

for opt; do
    case "$opt" in
        cryptroot=*)
            KOPT_cryptroot=${opt#cryptroot=}
            continue
            ;;
        cryptdm=*)
            KOPT_cryptdm=${opt#cryptdm=}
            continue
            ;;
        root=*)
            KOPT_root=${opt#root=}
            continue
            ;;
    esac
done

while [ ! -b /dev/mapper/${KOPT_cryptdm} ]; do
    /sbin/nlplug-findfs -c ${KOPT_cryptroot} -m ${KOPT_cryptdm} -p /sbin/mdev ${KOPT_root}
done

/usr/bin/killall sshd
